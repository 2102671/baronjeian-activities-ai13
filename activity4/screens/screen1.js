import * as React from 'react';
import {View , Image ,Text , StyleSheet , TouchableOpacity} from 'react-native';

const Screen1 = ({ navigation }) => {
    return (
      <View style={styles.main_layout}>
        <Image style={{width: '100%', height:300,}} source={require('../screens/whyIT/IT1.png')}></Image>
        <View style={styles.text_layout}>
          <Text style={{color:'black',fontSize: 20,fontStyle: 'italic'}}>
            I choose dream world beacuse my dream is that people would care about people's feelings.l mean like before you rob a bank you would think about the outcome of how the business will be affected.lf we were to care about cause and effect more the wouldn't be even half as many crimes as their are now.If people would realize that most of people had a families to go home to.l think that if we would care a little bit more our world could be longer.
          </Text>
        </View>
        <View style={{justifyContent: 'flex-end', flex: 1}}>
          <TouchableOpacity style={styles.btn_layout}
            onPress={() =>
            navigation.navigate('Screen2')}>
            <Text style={styles.btn_txt}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
    
  const styles = StyleSheet.create({
    main_layout: {
      flex: 1,
      padding:5,
      paddingTop: 30,
      alignItems: "center",
    },
    text_layout:{
      width: '100%',
      alignItems: "center",
      borderColor: "black",
      marginTop: 10,
      padding: 10,
    },
    btn_txt: {
      fontSize: 20,
      color: 'white',
    },
    btn_layout:  {
      alignItems: "center",
      backgroundColor: 'black',
      width: 250, 
      height: 50,
      paddingVertical: 10,
    },
  });
export default Screen1;