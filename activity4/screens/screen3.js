import * as React from 'react';
import {View , Image ,Text , StyleSheet , TouchableOpacity} from 'react-native';

const Screen3 = ({ navigation }) => {
    return (
      <View style={styles.main_layout}>
        <Image style={{width: '100%', height:300,}} source={require('../screens/whyIT/IT3.png')}></Image>
        <View style={styles.text_layout}>
          <Text style={{color:'black',fontSize: 20,fontStyle: 'italic'}}>
            I will choose the book reading that l want to read.And will set the time in a day for readings.
          </Text>
        </View>
        <View style={{justifyContent: 'flex-end', flex: 1}}>
          <TouchableOpacity style={styles.btn_layout}
            onPress={() =>
            navigation.navigate('Screen4')}>
            <Text style={styles.btn_txt}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
    
  const styles = StyleSheet.create({
    main_layout: {
      flex: 1,
      padding:5,
      paddingTop: 30,
      alignItems: "center",
    },
    text_layout:{
      width: '100%',
      alignItems: "center",
      borderColor: "black",
      marginTop: 10,
      padding: 10,
    },
    btn_txt: {
      fontSize: 20,
      color: 'white',
    },
    btn_layout:  {
      alignItems: "center",
      backgroundColor: 'black',
      width: 250, 
      height: 50,
      paddingVertical: 10,
    },
  });
export default Screen3;