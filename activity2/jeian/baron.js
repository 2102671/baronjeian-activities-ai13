import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { CheckBox, TouchableOpacity } from "react-native-web";

const Task = (props) => {
    return (
        <View style={styles.lists}>
            <View style={styles.list_add_me}>
                <View style={styles.circle}></View>
                <Text style={styles.list_text}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({

    circle: {
        width: 27,
        height: 27,
        backgroundColor: '#f5f2f4',
        borderWidth: 3,
        borderColor: '#0a0a0a',
        borderRadius: 60,
        marginRight: 15,
    },
    lists: {
        backgroundColor: '#f707cb',
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingVertical: 40,
    },
    list_add_me: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    list_text: {
        maxWidth: '85%',
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 18,
    },
});
export default Task;