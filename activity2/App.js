import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Keyboard, ScrollView } from 'react-native';
import Task from './jeian/baron';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function App() {

    return (
        <NavigationContainer>
        <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = 'home';
              } else if (route.name === 'Settings') {
                iconName = 'settings';
              } else if (route.name === 'Complete') {
                iconName = 'woman';
              }
  
              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: 'red',
            tabBarInactiveTintColor: 'black',
          })}
        >
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Settings" component={SettingsScreen} />
          <Tab.Screen name="Complete" component={CompleteScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    flex: 1,
    position: 'relative',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  scroller: {
    marginTop: 20,
    width: '90%',
  },

  jeian_btn: {
    paddingHorizontal: 16,
    backgroundColor: '#000000',
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
  },

  btn_add_d: {
    marginTop: 20,
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: '#000000',
    borderColor: '#000',
    borderWidth: 2,
    borderRadius: 10,
    width: '90%',
  },

  all_txt: {
    paddingVertical: 16,
    paddingHorizontal: 16,
    backgroundColor: '#FFF',
    borderColor: '#000',
    borderWidth: 2,
    borderRadius: 6,
    width: '90%',
  },
});

function HomeScreen() {
    const [task, all_task] = useState();
    const [taskItems, myTask_items] = useState([]);
    const handleAddTask = () => {
        Keyboard.dismiss();
        myTask_items([...taskItems, task])
        all_task("");
    }
    const completed_task = (index) => {
        let itemsCopy = [...taskItems];
        itemsCopy.splice(index, 1);
        myTask_items(itemsCopy)
    }
    return (
        <View style={styles.container}>
        <TextInput style={styles.all_txt} placeholder={'Write a task'} value={task} onChangeText={text => all_task(text)} />
        <View style={styles.btn_add_d}>
            <TouchableOpacity onPress={() => handleAddTask()}>
                <Text style={styles.jeian_btn}>Add Task</Text>
            </TouchableOpacity>
        </View>
        <ScrollView style={styles.scroller} contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps='handled'>
            <View style={styles.items}>
                {
                    taskItems.map((item, index) => {
                        return (
                            <TouchableOpacity key={index} onPress={() => completed_task(index)}>
                                <Task text={item} />
                            </TouchableOpacity>
                        )
                    })
                }
            </View>
        </ScrollView>
    </View>
    );
  }
  
  function SettingsScreen() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
    
  function CompleteScreen() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Completed Tasks Go Here</Text>
      </View>
    );
  }
  
  const Tab = createBottomTabNavigator();